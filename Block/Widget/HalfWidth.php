<?php
namespace Swiss\Blocks\Block\Widget;

// use Magento\Cms\Block\Widget\Block;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Widget\Block\BlockInterface;

class HalfWidth extends Template implements BlockInterface
{
    /**
     * @var string
     */
    protected $_template = 'Swiss_Blocks::widget/static_block/default.phtml';

    /**
     * @var int
     */
    private $_blockId;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context, Registry $registry, $data = []
    ) {
        $this->_blockId = (int) $data['block_id'];
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * @return \Magento\Cms\Model\Block
     */
    public function getBlock()
    {
        $objectManager = ObjectManager::getInstance();
        $blocks        = $objectManager->create('\Magento\Cms\Model\Block');
        $block         = $blocks->load($this->_blockId, 'block_id');
        return $block;
    }
}
